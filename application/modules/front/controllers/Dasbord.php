<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbord extends MX_Controller {
	public $data = [];
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in())
		{
			redirect('/','refresh');
		}
		$this->data['users'] = $this->ion_auth->user()->row();
		$this->load->model('responbcModel');
	}
	private function parsingLog()
	{
		$data = $this->responbcModel->getLogResponLazada();
		if($data == FALSE)
		{
			return FALSE;
		}else {
		
			if (!empty($data['arrival'])) {
			$total = [];
			$new = [];
			$update = [];
				foreach ($data['arrival'] as $v) {
					array_push($total,$v['total']);
					array_push($new, $v['new']);
					array_push($update,$v['update']);
				}
			$total = array_sum($total);
			$new = array_sum($new);
			$update = array_sum($update);
			$persen = round($new*100/$total,2);
			$arrival = compact('total','new','update','persen');
			}
			if(!empty($data['send_bc_success']))
			{
			$total = [];
			$new = [];
			$update = [];
				foreach ($data['send_bc_success'] as $v) {
					array_push($total,$v['total']);
					array_push($new, $v['new']);
					array_push($update,$v['update']);
				}
				$total = array_sum($total);
				$new = array_sum($new);
				$update = array_sum($update);
				$persen = round($new*100/$total,2);
				$send_bc_success = compact('total','new','update','persen');
			}
			if(!empty($data['handovered']))
			{
			$total = [];
			$new = [];
			$update = [];
				foreach ($data['handovered'] as $v) {
					array_push($total,$v['total']);
					array_push($new, $v['new']);
					array_push($update,$v['update']);
				}
				$total = array_sum($total);
				$new = array_sum($new);
				$update = array_sum($update);
				$persen = round($new*100/$total,2);
				$handovered = compact('total','new','update','persen');
			}
			return compact('arrival','send_bc_success','handovered');
		}
		
	}
	public function lazadaTracking()
	{
			$this->data['statLazada'] = $this->responbcModel->getLazadaStatus();
			$this->data['logLazada'] = $this->parsingLog();
			$this->data['title'] = "Lazda Tracking";
			$this->data['css'] = ['materialize.css','style.css','custom.css'];
			$this->data['css_plugin'] = [
				$this->parser->theme_url('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'),
			];
			$this->data['js'] = ['admin.js','demo.js','my_app.js'];
			$this->data['js_plugin'] = [
				$this->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js'),
			];
			
			$this->parser->parse('respon/lazadaTracking.tpl',$this->data);
	}
	public function log_respon()
	{
			// $this->data['stat'] = $this->responbcModel->getRespon();
			$this->data['lognya'] = $this->responbcModel->getLogRespon(); 
			$this->data['title'] = "Response BC";
			$this->data['css'] = ['materialize.css','style.css','custom.css'];
			$this->data['css_plugin'] = [
				// $this->parser->theme_url('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'),
			];
			$this->data['js'] = ['admin.js','my_app.js'];
			$this->data['js_plugin'] = [
				// $this->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/jquery.dataTables.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/jszip.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js'),
			];
			$this->parser->parse('respon/bc_respon.tpl',$this->data);
	}
	public function data_chart()
	{
		return $this->output
		        ->set_content_type('application/json')
		        ->set_output(json_encode(array('data'=>'1')));
	}
	public function cekstatus()
	{
			$this->data['title'] = "Cek Status";
			$this->data['css'] = ['materialize.css','style.css','custom.css'];
			$this->data['css_plugin'] = [
				// $this->parser->theme_url('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'),
			];
			$this->data['js'] = ['admin.js','my_app.js'];
			$this->data['js_plugin'] = [
				// $this->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/jquery.dataTables.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/jszip.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js'),
				// $this->parser->theme_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js'),
			];
		$this->parser->parse('respon/cekstatus.tpl',$this->data);
	}
}

/* End of file Dasbord.php */
/* Location: ./application/modules/front/controllers/Dasbord.php */