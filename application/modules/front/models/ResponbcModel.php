
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResponbcModel extends MY_Model {
public function __construct()
{
	parent::__construct();
	$this->mongo_db->reconnect([
				    'config' => [
				        'connection' => [
				            'host' => ['localhost'],
				            'port' => [],
				            'user_name' => '',
							'user_password' => '',
				            'db_name' => 'db_log_barangkiriman',
				        ]
				    ]
				]);
	
}
private function whereLike($collection='')
{

	$data = [];
	$collection = (new MongoDB\Client)->tracking_lazada->$collection;
	$cursor = $collection->find([
	    'status_date' => new MongoDB\BSON\Regex('^'.date('Y-m-d'), 'i')
	]);
	foreach ($cursor as $document) {
	   // printf("%s: %s, %s\n", $document['_id'], $document['status_date'], $document['tracking_number']);
		array_push($data, $document['status_date']);
	}
	return $data;
}
private function likeLogResponLazada($remark = '')
{

	$data = [];
	$collection = (new MongoDB\Client)->tracking_lazada->log_respon_lazada;
	$cursor = $collection->find([
	    'response.request_time' => new MongoDB\BSON\Regex('^'.date('Y-m-d'), 'i'),
	    'remark'=>$remark
	],['projection' => [
			'_id'=>0,
            'response.records.total' => 1,
            'response.records.new' => 1,
            'response.records.update' => 1

        ]]);


	foreach ($cursor as $document) {
		if(is_object($document['response']))
		{
			foreach ($document['response'] as $v) {
					array_push($data, (array)$v);
				}	
		}
		// array_push($data, iterator_to_array($document['response']));
	}
	return $data;
}

public function getLazadaStatus()
{
	$arr = $this->whereLike('send_arrival');
	$bc = $this->whereLike('send_bc_success');
	$hv = $this->whereLike('send_handovered');
	return ['arr'=>count($arr),'bc'=>count($bc),'hv'=>count($hv)];
}
public function getLogResponLazada()
{
	$handovered = $this->likeLogResponLazada('handovered');
	$send_bc_success = $this->likeLogResponLazada('bc_success');
	$arrival = $this->likeLogResponLazada('arrival');
	return compact('send_bc_success','handovered','arrival');
}
public function getRespon()
{
	$data = [
			[
				'no' => number_format($this->fetchDataRespon(1)),
				'icon' => 'all_inclusive',
				'bg' => 'bg-pink',
				'sts' => 'RESPON 100'
			],
			[
				'no' => number_format($this->fetchDataRespon(2)),
				'icon' => 'android',
				'bg' => 'bg-deep-purple',
				'sts' => 'RESPON 200'
			],
			[
				'no' => number_format($this->fetchDataRespon(3)),
				'icon' => 'assignment_late',
				'bg' => 'bg-teal',
				'sts' => 'RESPON 300'
			],
			[
				'no' => number_format($this->fetchDataRespon(4)),
				'icon' => 'attach_money',
				'bg' => 'bg-deep-orange',
				'sts' => 'RESPON 400'
			],
			[
				'no' => number_format($this->fetchDataRespon(5)),
				'icon' => 'beach_access',
				'bg' => 'bg-brown',
				'sts' => 'RESPON 500'
			],
			[
				'no' => number_format($this->fetchDataRespon(9)),
				'icon' => 'bookmark',
				'bg' => 'bg-blue-grey',
				'sts' => 'RESPON 900'
			],
			// [
			// 	'no' => number_format($this->fetchDataRespon('E')),
			// 	'icon' => 'cached',
			// 	'bg' => 'bg-black',
			// 	'sts' => 'RESPON ERR'
			// ],
	];
	

	return $data;
	

}
private function logResponBc($sts = '')
{
	
	$kemarin = strtotime("-24 hours");
	$sekarang = strtotime('now');
	$jono = $this->mongo_db->where_beth_equal('date',$kemarin,$sekarang)
							->where('status',$sts)
							->count('log_message');
		return $jono;

}
private function fetchDataRespon($kdres = '')
{
	$collection = (new MongoDB\Client)->db_log_barangkiriman->tmp_bc_response;
	$cursor = $collection->find([
	    'KD_RESPON' => new MongoDB\BSON\Regex("/^".$kdres.'/'),
	    'WK_REKAM' => new MongoDB\BSON\Regex("/^".date('Y/m/d').'/')
	]);
	
	echo  json_encode($cursor);
}
public function getLogRespon()
{
	$data = [
			[
				'no' => number_format($this->logResponBc('sending')),
				'icon' => 'all_inclusive',
				'bg' => 'bg-pink',
				'sts' => 'STATUS SENDING JOB'
			],
			[
				'no' => number_format($this->logResponBc('getallrespon')),
				'icon' => 'devices',
				'bg' => 'bg-deep-orange',
				'sts' => 'STATUS GET ALL RESPON JOB'
			],
			[
				'no' => number_format($this->logResponBc('debug')),
				'icon' => 'devices',
				'bg' => 'bg-blue',
				'sts' => 'STATUS DATA KOSONG JOB'
			],
			// [
			// 	'no' => number_format($this->logResponBc('error')),
			// 	'icon' => 'devices',
			// 	'bg' => 'bg-deep-orange',
			// 	'sts' => 'STATUS GET ALL RESPON'
			// ],
			];
			return $data;
}
	

}

/* End of file ResponbcModel.php */
/* Location: ./application/modules/front/models/ResponbcModel.php */