 {extends file="home.tpl"}
 {block name=body}
     <!-- Chart Samples -->

            <div class="block-header">
                <h2>Status Data Lazada Proses Kirim</h2>
                {* {$statLazada|@print_r} *}
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-3 bg-red">
                        <div class="icon">
                            <div class="chart chart-bar">{date('Y-m-d')}</div>
                        </div>
                        <div class="content">
                            <div class="text">LAZADA ARRIVAL</div>
                            <div class="number">{number_format($statLazada['arr'])}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-3 bg-indigo">
                        <div class="icon">
                            <div class="chart chart-bar">{date('Y-m-d')}</div>
                        </div>
                        <div class="content">
                            <div class="text">LAZADA BC SUCCESS</div>
                            <div class="number">{number_format($statLazada['bc'])}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-3 bg-orange">
                        <div class="icon">
                            <div class="chart chart-pie">{date('Y-m-d')}</div>
                        </div>
                        <div class="content">
                            <div class="text">LAZADA HANDOVERED</div>
                            <div class="number">{number_format($statLazada['hv'])}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-3 bg-deep-orange">
                        <div class="icon">
                            <div class="chart chart-pie">{'% HAWB'}</div>
                        </div>
                        <div class="content">
                            <div class="text">TOTAL</div>
                            <div class="number">{number_format(($statLazada['arr']+$statLazada['bc']+$statLazada['hv'])/3,2)}</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Counter Examples -->
            <!-- Hover Zoom Effect -->
            <div class="block-header">
                <h2>STATUS RESPON LAZADA</h2>
            </div>
            {* {$logLazada|@print_r} *}
            {foreach from=$logLazada key=k item=v}
              
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-3 bg-pink hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">email</i>
                        </div>
                        <div class="content">
                            <div class="icon">
                                <div class="text">NEW DATA</div>
                            </div>
                            <div class="text">RESPON {strtoupper($k)}</div>
                            <div class="number">{number_format($v['new'])}</div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-3 bg-blue hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">devices</i>
                        </div>
                        <div class="content">
                            <div class="icon">
                                <div class="text">UPDATE DATA</div>
                            </div>
                            <div class="text">RESPON {strtoupper($k)}</div>
                            <div class="number">{number_format($v['update'])}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-3 bg-light-blue hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">access_alarm</i>
                        </div>
                        <div class="content">
                            <div class="icon">
                                <div class="text">TOTAL SEND</div>
                            </div>
                            <div class="text">RESPON {strtoupper($k)}</div>
                            <div class="number">{number_format($v['total'])}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-3 bg-cyan hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">gps_fixed</i>
                        </div>
                        <div class="content">
                            <div class="text">Persentase {strtoupper($k)}</div>
                            <div class="number">{number_format($v['persen'])}%</div>
                        </div>
                    </div>
                </div>
            </div>
            {/foreach}

            <!-- #END# Hover Zoom Effect -->
            <!-- #END# Chart Samples -->
 {/block}
