 {extends file="home.tpl"}
 {block name=body}
 	<!-- Widgets -->
            {* {$stat|@print_r} *}
            <div class="card">
                        <div class="body">
                            <form id="last">
                                <h2 class="card-inside-title">Last Response: <a href="{base_url('getResponByAwb')}">{base_url('getResponByAwb')}</a></h2>
                                <div class="row clearfix">
                                    <div class="col-sm-8">
                                        <div class="form-group form-group-lg">
                                            <div class="form-line">
                                                <input class="form-control" placeholder="hawb" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn bg-red btn-block btn-lg waves-effect">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <form id="track">
                                <h2 class="card-inside-title">Tracking Response: <a href="{base_url('serviceTracking')}">{base_url('serviceTracking')}</a></h2>
                                <div class="row clearfix">
                                    <div class="col-sm-8">
                                        <div class="form-group form-group-lg">
                                            <div class="form-line">
                                                <input class="form-control" placeholder="hawb" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn bg-red btn-block btn-lg waves-effect">Submit</button>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
            {* {$lognya|@print_r} *}
 {/block}
