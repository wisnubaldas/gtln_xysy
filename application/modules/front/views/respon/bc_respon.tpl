 {extends file="home.tpl"}
 {block name=body}
 	<!-- Widgets -->
            {* {$stat|@print_r} *}
            <div class="row">
                {foreach from=$lognya item=i key=k }
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect {$i['bg']}">
                            <div class="icon">
                                <i class="material-icons">{$i['icon']}</i>
                            </div>
                            <div class="content">
                                <div class="text">{$i['sts']}</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">{$i['no']}</div>
                            </div>
                        </div>
                    </div>
                 {/foreach}
            </div>
            {* {$lognya|@print_r} *}
 {/block}
