<!DOCTYPE html>
<html>

<head>
   <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Google Fonts -->
        {* <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"> *}
        <link href="{$this->parser->theme_url('css/material_font/material-icons.css')}" rel="stylesheet" type="text/css">
       {*  {$css|@var_dump} *}
        <title>{block name=title}{$title}{/block} GTLN</title>
        
        <link href="{$this->parser->theme_url('plugins/bootstrap/css/bootstrap.css')}" rel="stylesheet">
        <link href="{$this->parser->theme_url('plugins/node-waves/waves.css')}" rel="stylesheet">
        <link href="{$this->parser->theme_url('plugins/animate-css/animate.css')}" rel="stylesheet">

        {foreach from=$css_plugin key=k item=v}
          <link href="{$v}" rel="stylesheet">
        {/foreach}
        {foreach from=$css key=k item=v}
        {css({$v})}
        {/foreach}
        
        <link href="{$this->parser->theme_url('css/themes/all-themes.css')}" rel="stylesheet" type="text/css">
</head>

<body class="theme-light-blue">
    {* {$users->username|@var_dump} *}
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/">ADMINBSB - MATERIAL DESIGN</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
               
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{base_url()}assets/images/user.png" width="48" height="48" alt="{$users->first_name}" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{$users->first_name}</div>
                    <div class="email">{$users->username}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">content_copy</i>
                            <span>Log Record</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{base_url()}front/dasbord/log_respon">Respon BC</a>
                            </li>
                            <li>
                                <a href="{base_url()}front/dasbord/lazadaTracking">Tracking Lazada</a>
                            </li>
                            <li>
                                <a href="{base_url()}front/dasbord/cekstatus">Cek Status</a>
                            </li>
                            {* <li>
                                <a href="#">Blank Page</a>
                            </li>
                            <li>
                                <a href="#">404 - Not Found</a>
                            </li>
                            <li>
                                <a href="#">500 - Server Error</a>
                            </li> *}
                        </ul>
                    </li>
                </ul>
            </div>
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
    <ol class="breadcrumb breadcrumb-bg-pink clearfix">
        <li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">library_books</i> <strong>{$title}</strong></li>
    </ol>
        <div class="container-fluid">
            <div class="block-header">
                {nocache}
                {block name=body}{$body}{/block}
                {/nocache}
            </div>
        </div>
    </section>
    <script src="{$this->parser->theme_url('plugins/jquery/jquery.min.js')}"></script>
    <script src="{$this->parser->theme_url('plugins/bootstrap/js/bootstrap.js')}"></script>
    <script src="{$this->parser->theme_url('plugins/bootstrap-select/js/bootstrap-select.js')}"></script>
    <script src="{$this->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js')}"></script>
    <script src="{$this->parser->theme_url('plugins/node-waves/waves.js')}"></script>
   {foreach from=$js_plugin key=k item=v}
    <script src="{$v}"></script>
    {/foreach}
    {foreach from=$js key=k item=v}
    {js({$v})}
    {/foreach}
</body>

</html>
