<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Decoder extends MX_Controller {
	protected $server;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['10.0.0.29'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'db_exsysBc',
					        ]
					    ]
					]);
		$this->load->helper('file');
	}
	private function liat($arr = NULL)
	{
		return print_r('<pre>'.print_r($arr,true).'<pre>');
	}

	public function index()
	{
        $this->mongo_db->switch_db('db_exsysBc');
		$feeder = $this->mongo_db->get('feeder');
		$hosts_to_ping = $this->_cekFeeder($feeder);
		$data = $this->mongo_db->limit(100)->get('tmp_xml_bc_shipment');
		$jmlServer = $this->mongo_db->count('feeder');
		// $jmlServerAktif = $this->mongo_db->where('jobStatus','proses')->get('feeder');
		$serverIdup = $jmlServer - $hosts_to_ping;
		$jml = ceil(count($data)/$serverIdup);
		if(count($data) != 0)
		{
			$i = 0;
			$bagiServer = array_chunk($data,$jml);
			foreach($bagiServer as $key => $value)
			    {
			    	$i++;
			    	$stat = $feeder[$key];
			    	if ($stat['jobStatus'] == 'idle' && $stat['statusServer'] != 'down') {
							foreach ($value as $v) {
								$xx = array_merge($v,['flag_fed'=>$stat['feeder']]);
								$this->mongo_db->insert('xml_bc_shipment',$xx);
								$this->mongo_db->where('hawb', $v['hawb'])->delete('tmp_xml_bc_shipment');
							}
						$this->mongo_db
						    ->set([
						        'jobStatus' => 'proses',
						        'jobStored' => $stat['jobStored']+count($value),
						        'jobStart'  => date('Y-m-d H:i:s'),
						        'statusServer'  => 'on'
						    ])
						    ->where('feeder', $stat['feeder']);
						    $this->mongo_db->updateAll('feeder');
			    	}else {
			    		$this->mongo_db->set([
						        'jobStatus' => 'idle',
						        'statusServer'  => 'off'
						    ])
			    		    ->where('feeder', $stat['feeder']);
						    $this->mongo_db->updateAll('feeder');
			    	}
			    }
			    $this->_log('success','decoder');
		}
											
		// if(count($jmlServerAktif) == $jmlServer)
		// {
		// 	$this->mongo_db->set([
		// 				        'jobStatus' => 'idle',
		// 				        'statusServer'  => 'off'
		// 				    ]);
		// 				    $this->mongo_db->updateAll('feeder');
		// }

	}
	private function _cekFeeder($host = array())
	{
		$x = 0;
		for($i = 0; $i < count($host); $i++)
		{
			$xxx = exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host[$i]['localIP'])), $res, $rval);
			if($rval !== 0)
			{
				$x++;
				$this->mongo_db->set(['statusServer' => 'down'])->where('feeder',$host[$i]['feeder']);
				$this->mongo_db->updateAll('feeder');
			}else {
				$this->mongo_db->set(['statusServer' => 'on'])->where('feeder',$host[$i]['feeder']);
				$this->mongo_db->updateAll('feeder');
			}
		}
		return $x;
	}
	private function _log($log,$message)
	{

		$this->mongo_db->insert('log_bc',['log'=>$log,'message'=>$message,'date'=>strtotime('now')]);
	}

}

/* End of file Decoder.php */
/* Location: ./application/modules/api_bc/controllers/Decoder.php */