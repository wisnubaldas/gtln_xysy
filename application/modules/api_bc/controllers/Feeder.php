<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."modules/bc_clearance/controllers/Parsing_xml.php";
class Feeder extends MX_Controller {
		protected $user = "nararya123";
		protected $password = "567123";
		protected $token = "TnpNNU56ZzFOVGd5TFRFNA0K";
		protected $npwp = "739785582031000";
		protected $wsdl = APPPATH.'wsdl/WSBarangKirimanNew.wsdl';
		protected $client;
	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set('Asia/Jakarta');
        // Load XML writer library
        $this->load->library('MY_xml_writer');
        $this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['10.0.0.29'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'db_exsysBc',
					        ]
					    ]
					]);
        $this->load->helper('string');
		// seting client 
        $this->setting = array(
        		"exceptions" => true,
        		'trace' => 1,
        		'exceptions'=> 1,
        		'connection_timeout'=> 30,        
                'stream_context'=> stream_context_create(array('ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false, 
                    'allow_self_signed' => true 
                        )
                    )
                )
            );
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$this->idSender = $this->user.'^$'.$this->password;
	}

	public function index()
	{
		$host = gethostname();
		echo $host;
		if($host == 'app008')
		{
			$fedder = 'fd01';			
		}elseif ($host == 'app009') {
			$fedder = 'fd02';
		}elseif ($host == 'app010') {
			$fedder = 'fd03';
		}elseif ($host == 'app011') {
			$fedder = 'fd04';
		}elseif ($host == 'app014') {
			$fedder = 'fd05';
		}elseif ($host == 'app015') {
			$fedder = 'fd06';
		}elseif ($host == 'app016') {
			$fedder = 'fd07';
		}elseif ($host == 'app017') {
			$fedder = 'fd08';
		}elseif ($host == 'app018') {
			$fedder = 'fd09';
		}elseif ($host == 'app019') {
			$fedder = 'fd10';
		}elseif ($host == 'app020') {
			$fedder = 'fd11';
		}elseif ($host == 'app021') {
			$fedder = 'fd12';
		}elseif ($host == 'app022') {
			$fedder = 'fd13';
		}elseif ($host == 'app023') {
			$fedder = 'fd14';
		}elseif ($host == 'app024') {
			$fedder = 'fd15';
		}else {
			$fedder = '';
		}
		$result = $this->mongo_db->where(['flag_fed'=>$fedder,'flag_xml'=>1])->limit(10)->get('xml_bc_shipment');
		print_r('<br>');
		print_r($result);
		
		if(count($result) != 0)
		{
			$dataXML = [];
			$hawbnya = [];
			foreach ($result as $v) {
				$v = array_slice($v,0,-2);
				array_push($dataXML,(object)array_slice($v,1));
				array_push($hawbnya,$v['hawb']);
			}
			$xmlnya = "<CN_PIBK>";
			$xmlnya .= Generate::shipment_xml($dataXML);
			$xmlnya .= "</CN_PIBK>";
				try {
					$requestData = [
									"data"=>$xmlnya,
									"id"=>$this->idSender,
									"sign"=>$this->token
									];
				$this->respon_bc = $this->client->__soapCall("kirimData",["kirimData"=>$requestData]);
				$arr = [];
				$data = new SimpleXMLElement($this->respon_bc->return);
						foreach ($data->HEADER as $value) {
							array_push($arr,array_merge((array)$value,['flag'=>'default','idnya'=>(integer)date('YmdHis').$this->_randomDigit(5)]));
						}
				$insertId = $this->mongo_db->insertAll('tmp_bc_response',$arr);
				print_r($this->respon_bc->return);
				
				$this->mongo_db
					    ->set([
					        'flag_xml' => 2
					    ])->where_in('hawb', $hawbnya);
					$this->mongo_db->updateAll('xml_bc_shipment');

					$this->_log('success',$host,'send data');
				}catch (Exception $exception) {
						print_r($exception->getMessage());
						$this->_log('fail',$host,'timeout or error WSDL');
				}
		}else {
			$this->mongo_db
				    ->set([
				        'jobEnd'  => date('Y-m-d H:i:s'),
				        'jobStored'  => 0,
				        'statusServer'  => 'off'
				    ])
				    ->where('host', $host);
				    $this->mongo_db->updateAll('feeder');
				    $this->_log('idle',$host,'no data send');
		}

		
	}
	private function _randomDigit($digits)
	{
		return rand(pow(10, $digits-1), pow(10, $digits)-1);
	}
	private function _log($log,$message,$host)
	{

		$this->mongo_db->insert('log_feeder',['log'=>$log, 'message'=>$host, 'host'=>$message,'date'=>date('Y-m-d H:m:i')]);
	}

}

/* End of file Feeder.php */
/* Location: ./application/modules/api_bc/controllers/Feeder.php */