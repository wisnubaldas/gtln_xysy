<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!function_exists('idAju'))
{
    function idAju($data)
    {
        $idnya = [1,2,3,4];
        if ($data == 1 || $data == 2)
        {
            return 13;
        }
        if ($data == 4)
        {
            return 1;
        }
    }
}
if(!function_exists ('array_to_object'))
{
    function array_to_object($array) {
        return (object) $array;
    }
}

if(!function_exists ('cek_property'))
{
    function cek_property($data) {
        if(!empty($data))
        {
            return $data;
        }else{return '';}
    }
}


if(!function_exists('cekarray'))
{
    function cekarray($str)
    {
        // if (is_array($str) or ($str instanceof Traversable));
        if(is_array($str) === true)
        {
            return '';
        }else{
            return $str;
        }
    }
}
if(!function_exists('make_refnumber'))
    {
        function make_refnumber($label,$jml)
            {
                // $number = $this->get_refnum();
                // $num = $number[0]->number + 1;
                $num = mt_rand(100000, 999999);
                $refnum = $label.date('md').str_pad($num, $jml,'0',STR_PAD_LEFT);
                // print_r($refnum);
                // $this->tps_model->generate_refnum($num);
                return $refnum;
            }
    }

if(! function_exists('xml2array'))
    {
            function xml2array ( $xmlObject, $out = array () )
        {
            foreach ( (array) $xmlObject as $index => $node )
                $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
            return $out;
        }
    }

if(! function_exists('cekstatus_byno'))
{
    function cekstatus_byno($v)
    {
            $xml = '<CEK_STATUS>';
                $xml .= '<HEADER>';
                    $xml .= '<NPWP>'.str_replace(['.','-'],'',$v->Ident_kode_broker).'</NPWP>'; 
                    $xml .= '<NO_BARANG>'.$v->hawb.'</NO_BARANG>';
                    $xml .= '<TGL_HOUSE_BLAWB>'.str_replace(['.','-'],'/',$v->tglawb).'</TGL_HOUSE_BLAWB>';
                    $xml .= '<NO_BC11>'.$v->bc11.'</NO_BC11>';
                    $xml .= '<TGL_BC11>'.str_replace(['.','-'],'/',$v->tglbc).'</TGL_BC11>';
                    $xml .= '<NO_POS_BC11>'.$v->nopos.'</NO_POS_BC11>';
                    $xml .= '<NO_SUBPOS_BC11>'.$v->subpos.'</NO_SUBPOS_BC11>';
                    $xml .= '<NO_SUBSUBPOS_BC11></NO_SUBSUBPOS_BC11>';
                $xml .= '</HEADER>';
            $xml .= '</CEK_STATUS>';
        return $xml;
    }
}
if ( ! function_exists('xml_str'))
{
    function xml_str($v)
    {
        $xml = '<CN_PIBK>';
        $xml  .= '<HEADER>';
        $xml .= '<JNS_AJU>'.$v->Id_aju.'</JNS_AJU>';
        $xml .= '<KD_JNS_PIBK>'.'2'.'</KD_JNS_PIBK>';
        $xml .= '<NO_BARANG>'.$v->hawb.'</NO_BARANG>';
        $xml .= '<KD_KANTOR>'.$v->kdkntr.'</KD_KANTOR>';
        $xml .= '<KD_JNS_ANGKUT>4</KD_JNS_ANGKUT>';
        $xml .= '<NM_PENGANGKUT>'.$v->FlightName.'</NM_PENGANGKUT>';
        $xml .= '<NO_FLIGHT>'.$v->flightno.'</NO_FLIGHT>';
        $xml .= '<KD_PEL_MUAT>'.$v->negaraasal.'</KD_PEL_MUAT>';
        $xml .= '<KD_PEL_BONGKAR>'.$v->Destination.'</KD_PEL_BONGKAR>';
        $xml .= '<KD_GUDANG>GBGD</KD_GUDANG>';
        $xml .= '<NO_INVOICE>'.$v->invoice.'</NO_INVOICE>';
        $xml .= '<TGL_INVOICE>'.str_replace('-','/',$v->tglInvo).'</TGL_INVOICE>';
        $xml .= '<KD_NEGARA_ASAL>'.substr($v->negaraasal, 0,2).'</KD_NEGARA_ASAL>';
        $xml .= '<JML_BRG>'.$v->Package.'</JML_BRG>';
        $xml .= '<NO_BC11>'.$v->bc11.'</NO_BC11>';
        $xml .= '<TGL_BC11>'.str_replace('-','/',$v->tglbc).'</TGL_BC11>';
        $xml .= '<NO_POS_BC11>'.$v->nopos.'</NO_POS_BC11>';
        $xml .= '<NO_SUBPOS_BC11>'.$v->subpos.'</NO_SUBPOS_BC11>';
        $xml .= '<NO_SUBSUBPOS_BC11></NO_SUBSUBPOS_BC11>';
        $xml .= '<NO_MASTER_BLAWB>'.$v->mawb.'</NO_MASTER_BLAWB>';
        $xml .= '<TGL_MASTER_BLAWB>'.str_replace('-','/',$v->tglawb).'</TGL_MASTER_BLAWB>';
        $xml .= '<NO_HOUSE_BLAWB>'.$v->hawb.'</NO_HOUSE_BLAWB>';
        $xml .= '<TGL_HOUSE_BLAWB>'.str_replace('-','/',$v->tglawb).'</TGL_HOUSE_BLAWB>';
        $xml .= '<KD_NEG_PENGIRIM>'.$v->Origin.'</KD_NEG_PENGIRIM>';
        $xml .= '<NM_PENGIRIM>'.htmlspecialchars($v->nmkirim).'</NM_PENGIRIM>';
        $xml .= '<AL_PENGIRIM>'.htmlspecialchars($v->almkirim).'</AL_PENGIRIM>';
        $xml .= '<JNS_ID_PENERIMA>'.$v->idterima.'</JNS_ID_PENERIMA>';
        // $xml .= '<NO_ID_PENERIMA>'.str_replace(str_split('-,.'), "", $v->consignee_NPWP).'</NO_ID_PENERIMA>'; // npwp
        $xml .= '<NO_ID_PENERIMA>'.$v->No_idterima.'</NO_ID_PENERIMA>';
        $xml .= '<NM_PENERIMA>'.htmlspecialchars($v->consignee_name).'</NM_PENERIMA>'; // consignee
        $xml .= '<AL_PENERIMA>'.htmlspecialchars($v->consignee_address).'</AL_PENERIMA>';
        $xml .= '<TELP_PENERIMA>'.$v->consignee_phone.'</TELP_PENERIMA>';
        $xml .= '<JNS_ID_PEMBERITAHU>5</JNS_ID_PEMBERITAHU>'; // broker
        $xml .= '<NO_ID_PEMBERITAHU>'.str_replace(['.','-'],'',$v->Ident_kode_broker).'</NO_ID_PEMBERITAHU>';
        $xml .= '<NM_PEMBERITAHU>'.$v->broker_name.'</NM_PEMBERITAHU>';
        $xml .= '<AL_PEMBERITAHU>'.$v->broker_address.'</AL_PEMBERITAHU>';
        $xml .= '<NO_IZIN_PEMBERITAHU>'.$v->broker_ijin.'</NO_IZIN_PEMBERITAHU>';
        $xml .= '<TGL_IZIN_PEMBERITAHU>'.$v->broker_ijin_date.'</TGL_IZIN_PEMBERITAHU>';
        $xml .= '<KD_VAL>'.$v->currency_valuta.'</KD_VAL>';
        $xml .= '<NDPBM>'.$v->ndpbm.'</NDPBM>';
        $xml .= '<FOB>'.number_format($v->FOB, 2, '.', '').'</FOB>';
        $xml .= '<ASURANSI>'.number_format($v->insurance,2,'.','').'</ASURANSI>';
        $xml .= '<FREIGHT>'.number_format($v->FreightCost,2,'.','').'</FREIGHT>';
        // $xml .= '<CIF>'.intval(strval($v->FOB+$v->insurance+$v->FreightCost)).'</CIF>';
        $xml .= '<CIF>'.number_format($v->CIF,2,'.','').'</CIF>';
        $xml .= '<NETTO>'.$v->Weight.'</NETTO>';
        $xml .= '<BRUTO>'.$v->Weight.'</BRUTO>';
        // $xml .= '<TOT_DIBAYAR>'.$v->TotalTax.'</TOT_DIBAYAR>'; 
        $xml .= '<TOT_DIBAYAR>'.$v->TotalTax.'</TOT_DIBAYAR>'; 
        // $xml .= '<NPWP_BILLING>'.str_replace(str_split('-,.'), '', $v->consignee_NPWP).'</NPWP_BILLING>';
         $xml .= '<NPWP_BILLING>'.$v->consignee_NPWP.'</NPWP_BILLING>';
        $xml .= '<NAMA_BILLING>'.htmlspecialchars($v->consignee_name).'</NAMA_BILLING>';
        $xml .= '<HEADER_PUNGUTAN>';
                $xml .= '<PUNGUTAN_TOTAL>';
                    $xml .= '<KD_PUNGUTAN>1</KD_PUNGUTAN>';
                    $xml .= '<NILAI>'.number_format($v->bm_value,2,'.','').'</NILAI>'; // bm value
                $xml .= '</PUNGUTAN_TOTAL>';
                $xml .= '<PUNGUTAN_TOTAL>';
                    $xml .= '<KD_PUNGUTAN>2</KD_PUNGUTAN>';
                    $xml .= '<NILAI>'.number_format($v->pph_value,2,'.','').'</NILAI>'; // pph value
                $xml .= '</PUNGUTAN_TOTAL>';
                $xml .= '<PUNGUTAN_TOTAL>';
                    $xml .= '<KD_PUNGUTAN>3</KD_PUNGUTAN>';
                    $xml .= '<NILAI>'.number_format($v->ppn_value,2,'.','').'</NILAI>'; // ppn value
                $xml .= '</PUNGUTAN_TOTAL>';
                $xml .= '<PUNGUTAN_TOTAL>';
                    $xml .= '<KD_PUNGUTAN>4</KD_PUNGUTAN>';
                    $xml .= '<NILAI>'.number_format($v->ppnBM_value,2,'.','').'</NILAI>'; // ppn bm vALUE
                $xml .= '</PUNGUTAN_TOTAL>';
        $xml .= '</HEADER_PUNGUTAN>';
        $xml .= '<DETIL>';
            $xml .= '<BARANG>';
                $xml .= '<SERI_BRG>1</SERI_BRG>';
                $xml .= '<HS_CODE>'.$v->hsCode.'</HS_CODE>';
                $xml .= '<UR_BRG>'.htmlspecialchars($v->kindofGood).'</UR_BRG>';
                $xml .= '<KD_NEG_ASAL>'.substr($v->Origin, 0,2).'</KD_NEG_ASAL>';
                $xml .= '<JML_KMS>'.$v->Package.'</JML_KMS>';
                $xml .= '<JNS_KMS>PK</JNS_KMS>';
                $xml .= '<CIF>'.number_format($v->CIF,2,'.','').'</CIF>';
                $xml .= '<KD_SAT_HRG>PK</KD_SAT_HRG>';
                $xml .= '<JML_SAT_HRG>'.$v->Package.'</JML_SAT_HRG>';
                $xml .= '<FL_BEBAS></FL_BEBAS>';
                $xml .= '<NO_SKEP></NO_SKEP>';
                $xml .= '<TGL_SKEP></TGL_SKEP>';
                    $xml .= '<DETIL_PUNGUTAN>';
                        $xml .= '<KD_PUNGUTAN>1</KD_PUNGUTAN>';
                        $xml .= '<NILAI>'.$v->bm_value.'</NILAI>'; // bm value
                        $xml .= '<KD_TARIF>1</KD_TARIF>';
                        $xml .= '<KD_SAT_TARIF>1</KD_SAT_TARIF>';
                        $xml .= '<JML_SAT>1</JML_SAT>';
                        $xml .= '<TARIF>'.$v->bm_persen.'</TARIF>';
                    $xml .= '</DETIL_PUNGUTAN>';
                    $xml .= '<DETIL_PUNGUTAN>';
                        $xml .= '<KD_PUNGUTAN>2</KD_PUNGUTAN>';
                        $xml .= '<NILAI>'.number_format($v->pph_value,2,'.','').'</NILAI>'; // pph value
                        $xml .= '<KD_TARIF>1</KD_TARIF>';
                        $xml .= '<KD_SAT_TARIF>1</KD_SAT_TARIF>';
                        $xml .= '<JML_SAT>1</JML_SAT>';
                        $xml .= '<TARIF>'.$v->pph_persen.'</TARIF>';
                    $xml .= '</DETIL_PUNGUTAN>';
                    $xml .= '<DETIL_PUNGUTAN>';
                        $xml .= '<KD_PUNGUTAN>3</KD_PUNGUTAN>';
                         $xml .= '<NILAI>'.number_format($v->ppn_value,2,'.','').'</NILAI>'; // ppn value
                        $xml .= '<KD_TARIF>1</KD_TARIF>';
                        $xml .= '<KD_SAT_TARIF>1</KD_SAT_TARIF>';
                        $xml .= '<JML_SAT>1</JML_SAT>';
                        $xml .= '<TARIF>'.$v->ppn_persen.'</TARIF>';
                    $xml .= '</DETIL_PUNGUTAN>';
                    $xml .= '<DETIL_PUNGUTAN>';
                        $xml .= '<KD_PUNGUTAN>4</KD_PUNGUTAN>';
                        $xml .= '<NILAI>'.number_format($v->ppnBM_value,2,'.','').'</NILAI>'; // ppn bm vALUE
                        $xml .= '<KD_TARIF>1</KD_TARIF>';
                        $xml .= '<KD_SAT_TARIF>1</KD_SAT_TARIF>';
                        $xml .= '<JML_SAT>1</JML_SAT>';
                        $xml .= '<TARIF>'.$v->ppnBM_persen.'</TARIF>';
                    $xml .= '</DETIL_PUNGUTAN>';
            $xml .= '</BARANG>';
        $xml .= '</DETIL>';
    $xml .= '</HEADER>';
    $xml .= '</CN_PIBK>';
        
    //     // print_r(htmlspecialchars($xml));
        return $xml;
    }
}
if ( ! function_exists('update_bc_str'))
{
    function update_bc_str($data)
    {

            $xml = '<PIBK_UPDATE>';
            foreach ($data as $v)
            {
                $xml .= '<HEADER>';
                $xml .= '<NO_BARANG>'.$v->hawb.'</NO_BARANG>';
                $xml .= '<TGL_HOUSE_BLAWB>'.str_replace(['.','-'],'/',$v->tglawb).'</TGL_HOUSE_BLAWB>';
                $xml .= '<NO_BC11>'.$v->bc11.'</NO_BC11>';
                $xml .= '<TGL_BC11>'.str_replace(['.','-'],'/',$v->tglbc).'</TGL_BC11>';
                $xml .= '<NO_POS_BC11>'.$v->nopos.'</NO_POS_BC11>';
                $xml .= '<NO_SUBPOS_BC11>'.$v->subpos.'</NO_SUBPOS_BC11>';
                $xml .= '<NO_SUBSUBPOS_BC11></NO_SUBSUBPOS_BC11>';
                $xml .= '</HEADER>';
            }
            $xml .= '</PIBK_UPDATE>';
            return $xml;
    }
}