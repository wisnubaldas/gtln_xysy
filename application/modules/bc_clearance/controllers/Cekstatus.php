<?php
/**
 * Created by PhpStorm.
 * User: wisnubaldas
 * Date: 5/16/2017
 * Time: 9:37 AM
 */
class Cekstatus extends CI_Controller{
    function __construct ()
    {
        parent::__construct();
        $this->load->helper('directory');
        // Load XML writer library
        $this->load->library('MY_xml_writer');
        $this->load->model('statusMdl');
        $this->load->helper('string');
        // seting client
        $this->wsdl = base_url('asset/wsdl/WSBarangKirimanNew.wsdl');
        $this->setting = array(
            'stream_context'=> stream_context_create(array('ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false,
                    'allow_self_signed' => true
                    )
                )
            )
        );
        $this->client = new SoapClient($this->wsdl, $this->setting);
        // user password buat api
        $user="nararya123";
        $pass="567123";
        $this->idSender = $user.'^$'.$pass;
    }
    function index()
    {
        $data = $this->input->get('hawb');
        $hostnya = $this->statusMdl->get_hawb($data);
        if (count($hostnya) == '') {
            echo "No data";
            return true;
        }else{
                
            foreach($hostnya as $v)
            {
                $this->xmlstring = cekstatus_byno($v);
                try
                {
                    $requestData = array(
                        "data" => $this->xmlstring,
                        "id"   => $this->idSender,
                        "sign" => "TnpNNU56ZzFOVGd5TFRFMw0K"
                    );
                    // kirim datanya via API
                    $response = $this->client->__soapCall( "getResponByAwb", array( "getResponByAwb" => $requestData ) );
                    $this->xml_respon = new SimpleXMLElement( $response->return );
                    // write_file('./asset/wsdl/cek_status_respon/'.$data.'.xml', $response->return);
                    $this->statusMdl->tes($this->xml_respon);

                    // keluarin respon xml 
                    $this->output
                        ->set_content_type('application/xml')
                        ->set_output($response->return);

                }catch( SoapFault $exception )
                {
                    echo $exception;
                }
            }
        }
    }
public function serviceTracking()
    {
      $hawb = $this->input->get('hawb');
      if ($hawb == '') {
          $this->output
            ->set_content_type('application/xml')
            ->set_output("<message>Isi Host Awbnya</message>");
      }
      try{
      $requestData = array(
                  "noAwb" => $hawb,
                   "npwp" => "739785582031000",
                   "id"   => $this->idSender,
                   "sign" => "TnpNNU56ZzFOVGd5TFRFMw0K");
      $response = $this->client->__soapCall("serviceTracking", array("serviceTracking" => $requestData));
            $xmlRespon = new SimpleXMLElement($response->return);
            foreach ($xmlRespon as $v) {
              if (substr($v->KD_RESPON,0,1) == '1') {$table = 'bc_respone_100';}
              if (substr($v->KD_RESPON,0,1) == '2') {$table = 'bc_respone_200';}
              if (substr($v->KD_RESPON,0,1) == '3') {$table = 'bc_respone_300';}
              if (substr($v->KD_RESPON,0,1) == '4') {$table = 'bc_respone_400';}
              if (substr($v->KD_RESPON,0,1) == '5') {$table = 'bc_respone_500';}
              if (substr($v->KD_RESPON,0,1) == '9') {$table = 'bc_respone_900';}
              if (!in_array(substr($v->KD_RESPON,0,1),['1','2','3','4','5','9'])) {$table = 'bc_respone';}

              $return = $this->statusMdl->insertTrackng($v,$table);
              // print_r($return);
            }
            // $this->output
            // ->set_content_type('application/xml')
            // ->set_output($response->return);
            print_r($response->return);
            
            }catch(SoapFault $exception)
              {
                echo $exception;
              }
        }

    public function mapingDirekroty()
    {
         $folder = 'archive-'.date('Y-m-d');
         $data = [];
         $sendDataXML = directory_map('/var/www/html/web/api_tlc/asset/xmlfile/sendData/');
          foreach ($sendDataXML as $v) {
                    $xml = file_get_contents('/var/www/html/web/api_tlc/asset/xmlfile/sendData/'.$v);
                    $objData = simplexml_load_string($xml);
                     array_push($data, $objData->HEADER);
         }
         $dataRespon = directory_map('/var/www/html/web/api_tlc/asset/xmlfile/getAllRespon/');
         foreach ($dataRespon as $v) {
                    $xml = file_get_contents('/var/www/html/web/api_tlc/asset/xmlfile/getAllRespon/'.$v);
                    $objData = simplexml_load_string($xml);
                    foreach ($objData->RESPONSE as $v) {
                     array_push($data, $v->HEADER);
                    }
         }
          $client     = new GuzzleHttp\Client();
          $url        = 'http://116.206.196.39:8080/api/decoder/gtln';
          $postData = ['form_params' => ['data' => json_encode($data)]];
          if(count($data) == 0)
          {
            log_message('error', 'No data Send decoder');
          }else{
             try {
            $response = $client->request('POST', $url,$postData);
            log_message('error', 'Send xml ke api decoder = '.$response->getBody());
            echo $response->getBody();
            } catch (GuzzleHttp\Exception\BadResponseException $e) {
              #guzzle repose for future use
              $response = $e->getResponse();
              $responseBodyAsString = $response->getBody()->getContents();
              log_message('error', $responseBodyAsString);
            }
          }
         
    }
    public function cekulangXml()
    {
        $url = $this->input->get('dirnya');
        if($url == '')
        {
          echo "parsing ulang file XML";
          echo "<br>";
          echo "input parameter dirnya by tanggal ";
          echo "<br>";
          echo "Contoh ?dirnya=2017-08-08";
        }else{
            $dir = '/home/wisnu/file/backupXml/getAllRespon/archive-'.$url.'/getAllRespon/';
            $dir2 = '/home/wisnu/file/backupXml/sendData/archive-'.$url.'/sendData/';

          if($url)
          {
            $nameXML = directory_map($dir);
            $sendData = directory_map($dir2);
            if(!$nameXML || !$sendData)
            {
              echo 'tidak ada tanggal tsb';
              return true;
            }

            foreach ($sendData as $v) {
              $this->statusMdl->insertFileName($v,$url,'sendData');
            }

            foreach ($nameXML as $v) {
                $this->statusMdl->insertFileName($v,$url,'getAllRespon');
            }
            echo "data cek ulang xml per hari sedang dalam prosess....";
            echo "<br>";
            echo "http://192.168.88.22:8080/cekstatus/runCekXML";
          }
        }
    }
    public function runCekXML()
    {
      $xmlName = $this->statusMdl->getFileXml();
      $data = [];
      foreach ($xmlName as $v) {
        if($v->status_xml == 'sendData')
          {
            $dir = '/home/wisnu/file/backupXml/sendData/archive-'.$v->tanggal.'/sendData/'.$v->nama_file;
            
             if (file_exists($dir)) {
                  $xml = file_get_contents($dir);
                  $objData = simplexml_load_string($xml);
                        array_push($data, $objData->HEADER);
                  $this->statusMdl->updateFlagFielXml($v->id);
                } else {
                    log_message('error', 'XML belom ke copy');
                }
          }
        if($v->status_xml == 'getAllRespon') 
          {
          $dir = '/home/wisnu/file/backupXml/getAllRespon/archive-'.$v->tanggal.'/getAllRespon/'.$v->nama_file;
          if (file_exists($dir)) {
                  $xml = file_get_contents($dir);
                  $objData = simplexml_load_string($xml);
                  foreach ($objData->RESPONSE as $conten) {
                      array_push($data, $conten->HEADER);
                  }
                  $this->statusMdl->updateFlagFielXml($v->id);
                } else {
                    log_message('error', 'XML belom ke copy');
                }
          }
        }
          $client     = new GuzzleHttp\Client();
          $url        = 'http://116.206.196.39:8080/api/decoder/gtln';
          $postData = ['form_params' => ['data' => json_encode($data)]];
          if(count($data) == 0)
          {
            log_message('error', 'No data Send decoder');
          }else{
             try {
            $response = $client->request('POST', $url,$postData);
            log_message('error', 'Cek ulang XML = '.$response->getBody());
            echo $response->getBody();
            } catch (GuzzleHttp\Exception\BadResponseException $e) {
              #guzzle repose for future use
              $response = $e->getResponse();
              $responseBodyAsString = $response->getBody()->getContents();
              log_message('error', $responseBodyAsString);
            }
          }
    }

}