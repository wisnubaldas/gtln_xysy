<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	/** ############
	 **	GetRespon.php create by: wisnu baldas
	 ** dir: /home/wisnu/Documents/web/api_xsys/application/modules/bc_clearance/controllers/GetRespon.php
     *  ############
     * tarik respon dari BC
     **/
require APPPATH."modules/bc_clearance/controllers/Parsing_xml.php";
class GetRespon extends MX_Controller {
	protected $user = "hkusuma";
	protected $password = "exsyst123";
	protected $token = "TURNeE1EQXdNekF3TFRFNA0K";
	protected $npwp = "031000300034000";
	protected $wsdl = APPPATH.'wsdl/WSBarangKirimanNew.wsdl';
	protected $client;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->library('MY_xml_writer');
		$model = ['bc_t_shipment_model','bc_respone_model','bc_respone_100_model','bc_respone_200_model','bc_respone_300_model','bc_respone_400_model','bc_respone_500_model','bc_respone_900_model','bc_respone_ERR_model','bc_respone_bc11_model'];
        $this->load->model($model);
        $this->load->helper('string');
        $this->setting = array(
        		"exceptions" => true,
        		'trace' => 1,
        		'exceptions'=> 1,
        		'connection_timeout'=> 30,        
                'stream_context'=> stream_context_create(array('ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false, 
                    'allow_self_signed' => true 
                        )
                    )
                )
            );
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$this->idSender = $this->user.'^$'.$this->password;
	}

public function get_all_response()
	{
		
	try{
		$requestData = array(
			"npwp" => $this->npwp,
			"id" => $this->idSender,
			"sign" => $this->token
			);
				$response = $this->client->__soapCall("requestRespon", array("requestRespon" => $requestData));
				$xml = '<ROOT>';
				$xml .= $response->return;
				$xml .= '</ROOT>';
				// $response = file_get_contents(APPPATH.'wsdl/getallrespon.xml');
				// print_r($response->return);
				if($response->return != '')
				{
					$xml = new SimpleXMLElement($xml);
					$hawb = [];
					foreach ($xml as $value) {
						$return = $this->bc_t_shipment_model->insert_getAll((array)$value->HEADER);
						array_push($hawb,(string)$value->HEADER->NO_BARANG);
					}
					$this->bc_t_shipment_model->log_record(['status'=>'getallrespon','message'=>implode(',',$hawb),'date'=>(int)strtotime('now')]);
				}
		}catch (SoapFault $exception) {
				   $this->bc_t_shipment_model->log_record(['status'=>'error',
						'message'=>$exception->getMessage(),
						'error_date'=>date('Y-m-d H:i:s'),
						'date'=>(int)strtotime('now')]);
		} 
	}
	public function paralel_respon()
	{

		for ($x = 0; $x <= 15; $x++) {
			$this->bc_t_shipment_model
				->log_record(['status'=>'getallrespon',
								'message'=>'ratikan-->'.$x,
								'timelog'=>date('Y-m-d H:i:s'),
								'date'=>(int)strtotime('now')]);
	   			 $xx = $this->get_all_response();
	   			 if($xx == NULL)
	   			 {
	   			 	echo $x;
	   			 	exit();
	   			 }
			} 
	}
}

/* End of file GetRespon.php */
/* Location: ./application/modules/bc_clearance/controllers/GetRespon.php */