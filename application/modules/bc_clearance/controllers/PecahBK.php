<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PecahBK extends MX_Controller {

	protected $user = "hkusuma";
	protected $password = "exsyst123";
	protected $token = "TURNeE1EQXdNekF3TFRFMw0K";
	protected $npwp = "031000300034000";
	protected $wsdl = APPPATH.'wsdl/WSBarangKirimanNew.wsdl';
	protected $client;

	function __construct ()
    {
        parent::__construct();
        $this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['localhost'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'db_log_barangkiriman',
					        ]
					    ]
					]);

        date_default_timezone_set('Asia/Jakarta');
        // Load XML writer library
        $this->load->library('MY_xml_writer');
        $model = ['pecahModel'];
        $this->load->model($model);
        $this->load->helper('string');
		// seting client 
        $this->setting = array(
        		"exceptions" => true,
        		'trace' => 1,
        		'exceptions'=> 1,
        		'connection_timeout'=> 30,        
                'stream_context'=> stream_context_create(array('ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false, 
                    'allow_self_signed' => true 
                        )
                    )
                )
            );
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$this->idSender = $this->user.'^$'.$this->password;
    }

public function getShipment()
{
	$return = $this->pecahModel
			->on('GTLN')
			->fields('Id_aju,kdkntr,FlightName,flightno,negaraasal,Destination,invoice,tglInvo,Package,bc11,tglbc,nopos,subpos,subsubpos,mawb,tglawb,hawb,tglawb,Origin,nmkirim,almkirim,consignee_NPWP,consignee_name,consignee_address,consignee_phone,broker_name,broker_address,broker_ijin,currency_valuta,FOB,TotalTax,insurance,FreightCost,CIF,weight_original,Weight,ndpbm,bm_value,pph_value,ppn_value,ppnBM_value,hsCode,kindofGood,idterima,No_idterima,bm_persen,pph_persen,ppn_persen,ppnBM_persen,broker_ijin_date,Ident_kode_broker')
			->where('flag_xml',1)
			->limit(300)
			->as_array()
			->get_all();

	 if(count($return) == 0)
	 	{
	 	    echo 'data kosong';
	 	    exit();
	 	}
			$client     = new GuzzleHttp\Client();
			$url = 'http://116.206.197.1/exsys_pjt/api';
			$response = $client->request('POST', $url, ['json' => $return]);
			$body = $response->getBody();
				// Implicitly cast the body to a string and echo it
			// echo $body;
			print_r(json_decode($body));
			

}
public function getResponCloud()
{
	$client     = new GuzzleHttp\Client();
			$url = 'http://116.206.197.1/exsys_pjt/api';
			$response = $client->request('GET', $url);
			$body = $response->getBody();
			
			$data = json_decode($body);
			if(count($data) != 0)
			{
				foreach ($data as $v) {
				$this->mongo_db->insert('tmp_bc_response',(array)$v);
				}
			print_r($data);
			}

}


} // end class

/* End of file PecahBK.php */
/* Location: ./application/modules/bc_clearance/controllers/PecahBK.php */