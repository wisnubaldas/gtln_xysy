<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['title'] = 'Error Log';

        $data['clear'] = site_url('tool/error_log/clear');

        $data['log'] = '';

         // Current Filename;
        $file = FCPATH . 'application/logs/' . 'log-'.date('Y-m-d').'.php';
        $fileBackup = FCPATH . 'application/logs/' . rand().'-'.date('Y-m-d').'.log';

        if (file_exists($file)) {
            $size = filesize($file);

            if ($size >= 1242880) {
                $suffix = array(
                    'B',
                    'KB',
                    'MB',
                    'GB',
                    'TB',
                    'PB',
                    'EB',
                    'ZB',
                    'YB'
                );

                $i = 0;

                while (($size / 1024) > 1) {
                    $size = $size / 1024;
                    $i++;
                }

                $error_warning = 'Warning: Your error log file %s is %s!';

                $data['error_warning'] = sprintf($error_warning, basename($file), round(substr($size, 0, strpos($size, '.') + 4), 2) . $suffix[$i]); rename($file, $fileBackup);

            } else {

                 // Updated from comment

                $log = file_get_contents($file, FILE_USE_INCLUDE_PATH, null); 
                $lines = explode("\n", $log); 
                $content = implode("\n", array_slice($lines, 1)); 
                $data['log'] = $content;
            }
        }
		$this->load->view('welcome_message',$data);
        // $this->load->view('header', $data)
        // $this->load->view('error_log', $data);
        // $this->load->view('footer');
    }
		
	function tes()
    {
        $mo = $this->load->model('MoGtln');
        print_r($mo->index());
    }
	function get_log()
	{
		$this->filecontent = file_get_contents();
		print_r($this->filecontent);
	}
	
}