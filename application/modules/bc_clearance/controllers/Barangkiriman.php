<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."modules/bc_clearance/controllers/Parsing_xml.php";

class Barangkiriman extends MX_Controller {
	protected $user = "hkusuma";
	protected $password = "exsyst123";
	protected $token = "TURNeE1EQXdNekF3TFRFNA0K";
	protected $npwp = "031000300034000";
	protected $wsdl = APPPATH.'wsdl/WSBarangKirimanNew.wsdl';
	protected $client;

	function __construct ()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        // Load XML writer library
        $this->load->library('MY_xml_writer');
        $model = ['bc_t_shipment_model','bc_respone_model','bc_respone_100_model','bc_respone_200_model','bc_respone_300_model','bc_respone_400_model','bc_respone_500_model','bc_respone_900_model','bc_respone_ERR_model','bc_respone_bc11_model'];
        $this->load->model($model);
        $this->load->helper('string');
		// seting client 
        $this->setting = array(
        		"exceptions" => true,
        		'trace' => 1,
        		'exceptions'=> 1,
        		'connection_timeout'=> 30,        
                'stream_context'=> stream_context_create(array('ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false, 
                    'allow_self_signed' => true 
                        )
                    )
                )
            );
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$this->idSender = $this->user.'^$'.$this->password;
    }
public function index()
{
	$return = $this->bc_t_shipment_model
						->fields('Id_aju,kdkntr,FlightName,flightno,negaraasal,Destination,invoice,tglInvo,Package,bc11,tglbc,nopos,subpos,subsubpos,mawb,tglawb,hawb,tglawb,Origin,nmkirim,almkirim,consignee_NPWP,consignee_name,consignee_address,consignee_phone,broker_name,broker_address,broker_ijin,currency_valuta,FOB,TotalTax,insurance,FreightCost,CIF,weight_original,Weight,ndpbm,bm_value,pph_value,ppn_value,ppnBM_value,hsCode,kindofGood,idterima,No_idterima,bm_persen,pph_persen,ppn_persen,ppnBM_persen,broker_ijin_date,Ident_kode_broker')
						->where('hawb','xsys00002')
						->limit(1)
						->get_all();
	$xmlnya = "<CN_PIBK>";
	$xmlnya .= Generate::shipment_xml($return);
	$xmlnya .= "</CN_PIBK>";
	return $this->output
        ->set_content_type('application/xml')
        ->set_status_header(200)
        ->set_output($xmlnya);
}

public function paralelSatuan()
{
	$this->benchmark->mark('code_start');

	for ($x = 0; $x <= 10; $x++) {
   			 $xx = $this->shipmentSatuan();
		} 
	$this->benchmark->mark('code_end');
echo 'Low--->'.$this->benchmark->elapsed_time('code_start', 'code_end');
}
public function cek_sending()
{
	$hawb = $this->input->get('hawb');
	$return = $this->bc_t_shipment_model
						->fields('Id_aju,kdkntr,FlightName,flightno,negaraasal,Destination,invoice,tglInvo,Package,bc11,tglbc,nopos,subpos,subsubpos,mawb,tglawb,hawb,tglawb,Origin,nmkirim,almkirim,consignee_NPWP,consignee_name,consignee_address,consignee_phone,broker_name,broker_address,broker_ijin,currency_valuta,FOB,TotalTax,insurance,FreightCost,CIF,weight_original,Weight,ndpbm,bm_value,pph_value,ppn_value,ppnBM_value,hsCode,kindofGood,idterima,No_idterima,bm_persen,pph_persen,ppn_persen,ppnBM_persen,broker_ijin_date,Ident_kode_broker')
						->where('hawb','ID2100055813')
						->limit(1)
						->get_all();
	if (!$return) {
			$this->bc_t_shipment_model->log_record(['status'=>'debug',
									'message'=>'ngga ada data di kirim',
									'date'=>(int)strtotime('now')]);
			exit();
		}else{
			foreach ($return as $v) {
				$this->bc_t_shipment_model
							->where('hawb',$v->hawb)
							->update(['flag_xml'=>2]);
				$r = $this->bc_t_shipment_model->insert_shipment_mongo((array)$v);
				// print_r($r);
			}
		}
	$xmlnya = "<CN_PIBK>";
	$xmlnya .= Generate::shipment_xml($return);
	$xmlnya .= "</CN_PIBK>";
	// print_r($return[0]->hawb);
	try {
			$requestData = [
							"data"=>$xmlnya,
							"id"=>$this->idSender,
							"sign"=>$this->token
							];
		 $this->respon_bc = $this->client->__soapCall("kirimData",["kirimData"=>$requestData]);
		 print_r($this->respon_bc->return);

		 $arr = [];
			$data = new SimpleXMLElement($this->respon_bc->return);
			foreach ($data->HEADER as $value) {
				array_push($arr,array_merge((array)$value,['flag'=>'default']));
				if($value->KD_RESPON == 'ERR')
				{
					$this->bc_respone_ERR_model->insert($value);
					foreach ($return as $v) {
					$this->bc_t_shipment_model
							->where('hawb',$v->hawb)
							->update(['flag_xml'=>4]);
							// print_r($r);
					}
				}
				// else {
				// 	$this->bc_t_shipment_model
				// 			->on('GTLN')
				// 			->where('hawb',$value->NO_BARANG)
				// 			->update(['flag_xml'=>2]);
				// }
				// 	$this->bc_t_shipment_model->reset_connection();
			}
			$return = $this->bc_t_shipment_model->respon_mongo($arr);
			$this->bc_t_shipment_model->log_record(['status'=>'sending',
									'message'=>serialize($return),
									'date'=>(int)strtotime('now')]);

		} catch (Exception $e) {
			$this->bc_t_shipment_model->log_record(['status'=>'error','message'=>serialize($e),'date'=>(int)strtotime('now')]);
		}	
}

public function shipmentSatuan()
{
	$return = $this->bc_t_shipment_model
						// ->on('GTLN')
						->fields('Id_aju,kdkntr,FlightName,flightno,negaraasal,Destination,invoice,tglInvo,Package,bc11,tglbc,nopos,subpos,subsubpos,mawb,tglawb,hawb,tglawb,Origin,nmkirim,almkirim,consignee_NPWP,consignee_name,consignee_address,consignee_phone,broker_name,broker_address,broker_ijin,currency_valuta,FOB,TotalTax,insurance,FreightCost,CIF,weight_original,Weight,ndpbm,bm_value,pph_value,ppn_value,ppnBM_value,hsCode,kindofGood,idterima,No_idterima,bm_persen,pph_persen,ppn_persen,ppnBM_persen,broker_ijin_date,Ident_kode_broker')
						->where('flag_xml',1)
						->limit(1)
						->get_all();
	if (!$return) {
			$this->bc_t_shipment_model->log_record(['status'=>'debug',
									'message'=>'ngga ada data di kirim',
									'date'=>(int)strtotime('now')]);
			exit();
		}else{
			foreach ($return as $v) {
				$this->bc_t_shipment_model
							// ->on('GTLN')
							->where('hawb',$v->hawb)
							->update(['flag_xml'=>2]);
				$r = $this->bc_t_shipment_model->insert_shipment_mongo((array)$v);
				// print_r($r);
			}
		}
	$xmlnya = "<CN_PIBK>";
	$xmlnya .= Generate::shipment_xml($return);
	$xmlnya .= "</CN_PIBK>";
	// print_r($return[0]->hawb);
	try {
			$requestData = [
							"data"=>$xmlnya,
							"id"=>$this->idSender,
							"sign"=>$this->token
							];
		 $this->respon_bc = $this->client->__soapCall("kirimData",["kirimData"=>$requestData]);
		 print_r($this->respon_bc->return);

		 $arr = [];
			$data = new SimpleXMLElement($this->respon_bc->return);
			foreach ($data->HEADER as $value) {
				array_push($arr,array_merge((array)$value,['flag'=>'default']));
				if($value->KD_RESPON == 'ERR')
				{
					$this->bc_respone_ERR_model->insert($value);
					foreach ($return as $v) {
					$this->bc_t_shipment_model
							->where('hawb',$v->hawb)
							->update(['flag_xml'=>4]);
							// print_r($r);
					}
				}
				// else {
				// 	$this->bc_t_shipment_model
				// 			->on('GTLN')
				// 			->where('hawb',$value->NO_BARANG)
				// 			->update(['flag_xml'=>2]);
				// }
				// 	$this->bc_t_shipment_model->reset_connection();
			}
			$return = $this->bc_t_shipment_model->respon_mongo($arr);
			$this->bc_t_shipment_model->log_record(['status'=>'sending',
									'message'=>serialize($return),
									'date'=>(int)strtotime('now')]);

		} catch (Exception $e) {
			$this->bc_t_shipment_model->log_record(['status'=>'error','message'=>serialize($e),'date'=>(int)strtotime('now')]);
		}	
}

public function shipment()
    {
    	$this->db->trans_start();
    	$return = $this->bc_t_shipment_model
						->fields('Id_aju,kdkntr,FlightName,flightno,negaraasal,Destination,invoice,tglInvo,Package,bc11,tglbc,nopos,subpos,subsubpos,mawb,tglawb,hawb,tglawb,Origin,nmkirim,almkirim,consignee_NPWP,consignee_name,consignee_address,consignee_phone,broker_name,broker_address,broker_ijin,currency_valuta,FOB,TotalTax,insurance,FreightCost,CIF,weight_original,Weight,ndpbm,bm_value,pph_value,ppn_value,ppnBM_value,hsCode,kindofGood,idterima,No_idterima,bm_persen,pph_persen,ppn_persen,ppnBM_persen,broker_ijin_date,Ident_kode_broker')
						->where('flag_xml',1)
						// ->where('hawb',array('LOG170001032','LOG170001029','LOG170001033'))
						->limit(20)
						->get_all();
		// $xxx = $this->db->last_query();
						// $this->bc_t_shipment_model->reset_connection();
		if (!$return) {
			// echo 'string';
			$this->bc_t_shipment_model->log_record(['status'=>'debug',
									'message'=>'ngga ada data di kirim',
									'date'=>(int)strtotime('now')]);
			return true;
		}else{
			foreach ($return as $v) {
				$r = $this->bc_t_shipment_model->insert_shipment_mongo((array)$v);
				// print_r($r);
			}
		}
		
		$xmlnya = "<CN_PIBK>";
		$xmlnya .= Generate::shipment_xml($return);
		$xmlnya .= "</CN_PIBK>";

		// print_r($xmlnya);
		try {
			$requestData = [
							"data"=>$xmlnya,
							"id"=>$this->idSender,
							"sign"=>$this->token
							];
		 $this->respon_bc = $this->client->__soapCall("kirimData",["kirimData"=>$requestData]);

		
		}catch (Exception $exception) {
				$this->bc_t_shipment_model->log_record(['status'=>'error',
						'message'=>$exception->getMessage(),
						'error_date'=>date('Y-m-d H:i:s'),
						'date'=>(int)strtotime('now')]);
				// $error = $exception->getMessage();
				// $time_request = (microtime(true)-$time_start);
			 //    if(ini_get('default_socket_timeout') < $time_request) {
			 //        print_r($error);
			 //    } 
				print_r($exception->getMessage());
		}

			if ($this->respon_bc) {
				foreach ($return as $v) {
				$this->bc_t_shipment_model
							->where('hawb',$v->hawb)
							->update(['flag_xml'=>4]);
				// print_r($r);
				}
		print_r($this->respon_bc->return);
				$arr = [];
				$data = new SimpleXMLElement($this->respon_bc->return);
				foreach ($data->HEADER as $value) {
					array_push($arr,array_merge((array)$value,['flag'=>'default']));
					if($value->KD_RESPON == 'ERR')
					{
						$this->bc_respone_ERR_model->insert($value);
					}
					else {
						$this->bc_t_shipment_model
								->where('hawb',$value->NO_BARANG)
								->update(['flag_xml'=>2]);
					}
						// $this->bc_t_shipment_model->reset_connection();
				}
				$return = $this->bc_t_shipment_model->respon_mongo($arr);
				$this->bc_t_shipment_model->log_record(['status'=>'sending',
										'message'=>$return,
										'date'=>(int)strtotime('now')]);
			}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			{
			        $this->bc_t_shipment_model->log_record(['status'=>'error',
										'message'=>"error transaksi",
										'error_date'=>date('Y-m-d H:i:s'),
										'date'=>(int)strtotime('now')]);
			}

    }
public function paralel_respon()
{
	for ($x = 0; $x <= 15; $x++) {
   			 $xx = $this->get_all_response();
   			 sleep(5);
   			 echo 'sleep';
		} 
}
public function get_all_response()
	{
		
	try{
		$requestData = array(
			"npwp" => $this->npwp,
			"id" => $this->idSender,
			"sign" => $this->token
			);
				$response = $this->client->__soapCall("requestRespon", array("requestRespon" => $requestData));
				$xml = '<ROOT>';
				$xml .= $response->return;
				$xml .= '</ROOT>';
				// $response = file_get_contents(APPPATH.'wsdl/getallrespon.xml');
				print_r($response->return);

				$xml = new SimpleXMLElement($xml);
				$hawb = [];
				foreach ($xml as $value) {
					$return = $this->bc_t_shipment_model->insert_getAll((array)$value->HEADER);
					array_push($hawb,(string)$value->HEADER->NO_BARANG);
				}
				$this->bc_t_shipment_model->log_record(['status'=>'getallrespon','message'=>implode(',',$hawb),'date'=>(int)strtotime('now')]);
		}catch (SoapFault $exception) {
				   $this->bc_t_shipment_model->log_record(['status'=>'error',
						'message'=>$exception->getMessage(),
						'error_date'=>date('Y-m-d H:i:s'),
						'date'=>(int)strtotime('now')]);
		} 
	}
/** ############
 **	Barangkiriman.php create by: wisnu baldas
 ** dir: /home/wisnu/Documents/web/api_xsys/application/modules/bc_clearance/controllers/Barangkiriman.php
 *  ############
 * service update bc11 
 **/
public function update_bc11()
	{
		$return = $this->bc_t_shipment_model
						
						->fields('hawb,tglawb,bc11,tglbc,nopos,subpos')
						->where('flag_bc11',1)
						->limit(5)
						->get_all();
		if (!$return) {
			$this->bc_t_shipment_model->log_record(['status'=>'debug',
									'message'=>'ngga ada data di kirim',
									'date'=>(int)strtotime('now')]);
			return true;
		}

		try{
			$requestData = array(
					"data"=>Generate::bc11_xml($return),
					"id" => $this->idSender,
					"sign" => $this->token
					);
			$response = $this->client->__soapCall("updateBc11", array("updateBc11" => $requestData));
			
			}
		catch (SoapFault $exception) {
					$this->bc_t_shipment_model->log_record(['status'=>'error','message'=>serialize($exception),'date'=>(int)strtotime('now')]);
			} 
		if ($response) {
			$data = new SimpleXMLElement($response->return);
			$hawb = [];
			foreach ($data->HEADER as $value) {
				$return = $this->bc_t_shipment_model->respon_mongo((array)$value);
				array_push($hawb,(string)$value->NO_BARANG);
			}
			$this->bc_t_shipment_model->log_record(['status'=>'sending',
									'message'=>implode(',',$hawb),
									'date'=>(int)strtotime('now')]);
			foreach ($hawb as $value) {
				$this->bc_t_shipment_model
					->where('hawb',$value)
					->update(['flag_xml'=>1]);
					$this->bc_t_shipment_model->reset_connection();
			}
		}	
	}

	public function push_temp_respon()
	{
		$return  = $this->mongo_db
						->limit(500)
						->where(['flag'=>'default'])
						->get('tmp_bc_response');
		// print_r($return);

		if(count($return) == 0)
		{
			return true;
		}

		foreach ($return as $v) {

				$arr = array_slice($v, 1,-1,true);
					print_r(count($v));
				if(substr($arr['KD_RESPON'],0,1) == '1')
				{
					$id = $this->bc_respone_100_model->insert($arr);
					$this->bc_respone_100_model->reset_connection();
					if($id)
					{
					   $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}

				}elseif (substr($arr['KD_RESPON'],0,1) == '2') {
					$id = $this->bc_respone_200_model->insert($arr);
					$this->bc_respone_200_model->reset_connection();
					if($id)
					{
					 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}
				}elseif (substr($arr['KD_RESPON'],0,1) == '3') {
					if (array_key_exists('DETIL_PUNGUTAN', $arr)) {
						    $arr303 = array_slice($arr, 0,-1,true);
						    print_r($arr303);
						    $id = $this->bc_respone_300_model->insert($arr303);
						    if($id)
							{
							 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
							}
						}else {
							$id = $this->bc_respone_300_model->insert($arr);
							if($id)
							{
							 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
							}
						}
						$this->bc_respone_300_model->reset_connection();
					
				}elseif (substr($arr['KD_RESPON'],0,1) == '4') {
					$id = $this->bc_respone_400_model->insert($arr);
					$this->bc_respone_400_model->reset_connection();
					if($id)
					{
					 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}
				}elseif (substr($arr['KD_RESPON'],0,1) == '5') {
					$id = $this->bc_respone_500_model->insert($arr);
					$this->bc_respone_500_model->reset_connection();
					if($id)
					{
					 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}
				}elseif (substr($arr['KD_RESPON'],0,1) == '9') {
					$id = $this->bc_respone_900_model->insert($arr);
					$this->bc_respone_900_model->reset_connection();
					if($id)
					{
					 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}
				}elseif ($arr['KD_RESPON'] == 'ERR') {
					$id = $this->bc_respone_ERR_model->insert($arr);
					$this->bc_respone_ERR_model->reset_connection();
					if($id)
					{
					 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}
				}elseif ($arr['KD_RESPON'] == 101 || $arr['KD_RESPON'] == 301) {
					$id = $this->bc_respone_bc11_model->insert($arr);
					$this->bc_respone_bc11_model->reset_connection();
					if($id)
					{
					 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}
				}else {
					$id = $this->bc_respone_model->insert($arr);
					$this->bc_respone_model->reset_connection();
					if($id)
					{
					 $this->bc_t_shipment_model->update_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}
				}

		}
		
	}
	
} // end class
