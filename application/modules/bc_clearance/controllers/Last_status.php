<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."modules/bc_clearance/controllers/Parsing_xml.php";

class Last_status extends MX_Controller {
		protected $user = "hkusuma";
		protected $password = "exsyst123";
		protected $token = "TURNeE1EQXdNekF3TFRFMw0K";
		protected $npwp = "031000300034000";
		protected $wsdl = APPPATH.'wsdl/WSBarangKirimanNew.wsdl';
		protected $client;
	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set('Asia/Jakarta');
        // Load XML writer library
        $this->load->library('MY_xml_writer');
 		$model = ['bc_t_shipment_model','bc_respone_model','bc_respone_100_model','bc_respone_200_model','bc_respone_300_model','bc_respone_400_model','bc_respone_500_model','bc_respone_900_model','bc_respone_ERR_model','bc_respone_bc11_model'];
        $this->load->model($model);
        $this->load->helper('string');
		// seting client 
        $this->setting = array(
        		"exceptions" => true,        
                'stream_context'=> stream_context_create(array('ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false, 
                    'allow_self_signed' => true 
                        )
                    )
                )
            );
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$this->idSender = $this->user.'^$'.$this->password;
	}
public function requestRespon()
  {
    
  try{
    $requestData = array(
      "npwp" => $this->npwp,
      "id" => $this->idSender,
      "sign" => $this->token
      );
        $response = $this->client->__soapCall("requestRespon", array("requestRespon" => $requestData));
        $xml = '<ROOT>';
        $xml .= $response->return;
        $xml .= '</ROOT>';
        // $response = file_get_contents(APPPATH.'wsdl/getallrespon.xml');
        $xml = new SimpleXMLElement($xml);
        $hawb = [];
        foreach ($xml as $value) {
          $return = $this->bc_t_shipment_model->insert_getAll((array)$value->HEADER);
          array_push($hawb,(string)$value->HEADER->NO_BARANG);
        }
        $this->bc_t_shipment_model->log_record(['status'=>'getallrespon','message'=>implode(',',$hawb),'date'=>(int)strtotime('now')]);
        return $this->output
			        ->set_content_type('application/json')
			        ->set_status_header(200)
			        ->set_output(json_encode($xml));
    }catch (SoapFault $exception) {
           return $this->output
			        ->set_content_type('application/json')
			        ->set_status_header(200)
			        ->set_output(json_encode($exception));
           $this->bc_t_shipment_model->log_record(['status'=>'error','message'=>serialize($exception),'date'=>(int)strtotime('now')]);
    } 
  }

	public function serviceTracking()
	{
		  
		  $hawb = $this->input->get('hawb');
		  if($hawb == '')
			{
				echo 'hawb....?';
				return true;
			}
			try {
			  $requestData = array(
				       "noAwb" => $hawb,
				       "npwp" => $this->npwp,
				       "id"   => $this->idSender,
				       "sign" => $this->token);
		      $response = $this->client->__soapCall("serviceTracking", array("serviceTracking" => $requestData));

		      if ($response->return == '') {
		      	echo 'no data respon';
		      	exit();
		      }
		      $xmlParse = new SimpleXMLElement($response->return);

		      foreach ($xmlParse->HEADER as $v) {
		      	$v = (array)$v;

				if(substr($v['KD_RESPON'],0,1) == '1')
				{
					$id = $this->bc_respone_100_model->on('GTLN')
							->where('NO_BARANG',$v['NO_BARANG'])
							->where('KD_RESPON',$v['KD_RESPON'])
							->update($v);
					if($id == false)
					{
						$this->bc_respone_100_model->on('GTLN')->insert($v);
					}
				}elseif (substr($v['KD_RESPON'],0,1) == '2') {
					$id = $this->bc_respone_200_model->on('GTLN')
							->where('NO_BARANG',$v['NO_BARANG'])
							->where('KD_RESPON',$v['KD_RESPON'])
							->update($v);
					if($id == false)
					{
						$this->bc_respone_200_model->on('GTLN')->insert($v);
					}
				}elseif (substr($v['KD_RESPON'],0,1) == '3') {
					 if($v['KD_RESPON'] == '303')
					 {
					 	$v = array_splice($v, 0,-1);
					 }
					 	$id = $this->bc_respone_300_model->on('GTLN')
								->where('NO_BARANG',$v['NO_BARANG'])
								->where('KD_RESPON',$v['KD_RESPON'])
								->update($v);
						if($id == false)
						{
							$this->bc_respone_300_model->on('GTLN')->insert($v);
						}
					 
					
				}elseif (substr($v['KD_RESPON'],0,1) == '4') {
					
					$id = $this->bc_respone_400_model->on('GTLN')
							->where('NO_BARANG',$v['NO_BARANG'])
							->where('KD_RESPON',$v['KD_RESPON'])
							->update($v);
					if($id == false)
					{
						$this->bc_respone_400_model->on('GTLN')->insert($v);
					}
				}elseif (substr($v['KD_RESPON'],0,1) == '5') {
					$id = $this->bc_respone_500_model->on('GTLN')
							->where('NO_BARANG',$v['NO_BARANG'])
							->where('KD_RESPON',$v['KD_RESPON'])
							->update($v);
					if($id == false)
					{
						$this->bc_respone_500_model->on('GTLN')->insert($v);
					}
				}elseif (substr($v['KD_RESPON'],0,1) == '9') {
					$id = $this->bc_respone_900_model->on('GTLN')
							->where('NO_BARANG',$v['NO_BARANG'])
							->where('KD_RESPON',$v['KD_RESPON'])
							->update($v);
					if($id == false)
					{
						$this->bc_respone_900_model->on('GTLN')->insert($v);
					}
				}
				echo json_encode($v);
		      }
			} catch (Exception $e) {
				echo $e;
			}

	}
	public function index()
	{
		$data = $this->input->get('hawb');
		if($data == '')
		{
			echo 'hawb....?';
			return true;
		}
		$return = $this->bc_t_shipment_model
					->on('GTLN')
					->where('hawb',$data)
					->fields('hawb,tglawb,bc11,tglbc,nopos,subpos,Ident_kode_broker')
					->get();
		$data = Generate::xmlStatusAwb($return);
		try {
				$requestData = [
							"data"=>$data,
							"id"=>$this->idSender,
							"sign"=>$this->token
							];
		$this->respon_bc = $this->client->__soapCall("getResponByAwb",["getResponByAwb"=>$requestData]);
		$xmlParse = new SimpleXMLElement($this->respon_bc->return);
		$xmlParse = (array)$xmlParse->HEADER;
				// update jika ada house
		
				if(substr($xmlParse['KD_RESPON'],0,1) == '1')
				{
					$this->bc_respone_100_model->on('GTLN')->insert($xmlParse);

					// $id = $this->bc_respone_100_model->on('GTLN')->where('NO_BARANG',$xmlParse['NO_BARANG'])->update($xmlParse);
					// if($id == false)
					// {
					// 	$this->bc_respone_100_model->on('GTLN')->insert($xmlParse);
					// }
				}elseif (substr($xmlParse['KD_RESPON'],0,1) == '2') {
					$this->bc_respone_200_model->on('GTLN')->insert($xmlParse);
					// $id = $this->bc_respone_200_model->on('GTLN')->where('NO_BARANG',$xmlParse['NO_BARANG'])->update($xmlParse);
					// if($id == false)
					// {
					// 	$this->bc_respone_200_model->on('GTLN')->insert($xmlParse);
					// }
				}elseif (substr($xmlParse['KD_RESPON'],0,1) == '3') {
					$this->bc_respone_300_model->on('GTLN')->insert($xmlParse);

					// $id = $this->bc_respone_300_model->on('GTLN')->where('NO_BARANG',$xmlParse['NO_BARANG'])->update($xmlParse);
					// if($id == false)
					// {
					// 	$this->bc_respone_300_model->on('GTLN')->insert($xmlParse);
					// }
				}elseif (substr($xmlParse['KD_RESPON'],0,1) == '4') {
					$this->bc_respone_400_model->on('GTLN')->insert($xmlParse);
					
					// $id = $this->bc_respone_400_model->on('GTLN')->where('NO_BARANG',$xmlParse['NO_BARANG'])->update($xmlParse);
					// if($id == false)
					// {
					// 	$this->bc_respone_400_model->on('GTLN')->insert($xmlParse);
					// }
				}elseif (substr($xmlParse['KD_RESPON'],0,1) == '5') {
					$this->bc_respone_500_model->on('GTLN')->insert($xmlParse);
					// $id = $this->bc_respone_500_model->on('GTLN')->where('NO_BARANG',$xmlParse['NO_BARANG'])->update($xmlParse);
					// if($id == false)
					// {
					// 	$this->bc_respone_500_model->on('GTLN')->insert($xmlParse);
					// }
				}elseif (substr($xmlParse['KD_RESPON'],0,1) == '9') {
					$this->bc_respone_900_model->on('GTLN')->insert($xmlParse);
					// $id = $this->bc_respone_900_model->on('GTLN')->where('NO_BARANG',$xmlParse['NO_BARANG'])->update($xmlParse);
					// if($id == false)
					// {
					// 	$this->bc_respone_900_model->on('GTLN')->insert($xmlParse);
					// }
				}
				// echo json_encode($xmlParse);
				print_r($xmlParse);

		} catch (Exception $e) {
			$xxx = $this->bc_t_shipment_model->log_record(['status'=>'error','message'=>serialize($e),'date'=>(int)strtotime('now')]);

		}
	}

}

/* End of file Last_status.php */
/* Location: ./application/modules/bc_clearance/controllers/Last_status.php */