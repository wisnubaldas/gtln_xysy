<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bc_respone_100_model extends MY_Model {
public $table = 'bc_respone_100'; // you MUST mention the table name
	public $primary_key = 'id_res_cn'; // you MUST mention the primary key
	public $mongo_conn;


	public function __construct()
	{
		
		$this->timestamps = array('create_at','update_at');
		$this->return_as = 'object';
		parent::__construct();
		$this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['localhost'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'db_log_barangkiriman',
					        ]
					    ]
					]);

	}
}

/* End of file Bc_respone_model.php */
/* Location: ./application/modules/bc_clearance/models/Bc_respone_model.php */