<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PecahModel extends MY_Model {

	public $table = 'bc_t_shipment'; // you MUST mention the table name
	public $primary_key = 'noid'; // you MUST mention the primary key
	public $mongo_conn;

	public function __construct()
	{
		$this->timestamps = FALSE;
		$this->return_as = 'object';
		$this->after_get[] = 'flag_xml';
		parent::__construct();
		$this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['localhost'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'db_log_barangkiriman',
					        ]
					    ]
					]);

	}
	protected function flag_xml($data)
	{
		$return = [];
		foreach ($data as $value) {
			array_push($return,array_merge($value,['flag_xml'=>1]));
			$this->db->set('flag_xml',5);
			$this->db->where('hawb',$value['hawb']);
			$this->db->update('bc_t_shipment');
		}
		return $return;
	}


}

/* End of file PecahModel.php */
/* Location: ./application/modules/bc_clearance/models/PecahModel.php */