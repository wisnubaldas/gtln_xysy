<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bc_t_shipment_model extends MY_Model {
	public $table = 'bc_t_shipment'; // you MUST mention the table name
	public $primary_key = 'noid'; // you MUST mention the primary key
	public $mongo_conn;

	public function __construct()
	{
		$this->timestamps = FALSE;
		$this->return_as = 'object';
		$this->after_get[] = 'updateShipment';
		parent::__construct();
		$this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['localhost'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'db_log_barangkiriman',
					        ]
					    ]
					]);

	}
	protected function updateShipment($data)
	{
		$hostnya = [];
		foreach ($data as $v) {
			array_push($hostnya,$v['hawb']);
		}
			$this->db->set('flag_xml', 6, FALSE);
			$this->db->where_in('hawb', $hostnya);
			$this->db->update('bc_t_shipment'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
		return $data;
	}
	public function update_err($wkrespon,$kdrespon)
	{
		$this->mongo_db
			    ->set(['flag' => 'mysql'])
			    ->where('WK_RESPON', $wkrespon)
			    ->where('KD_RESPON', $kdrespon);
			    $updateStatus = $this->mongo_db->updateAll('tmp_bc_response');
			    return $updateStatus;
	}
	public function update_respon($id)
	{
		$this->mongo_db
			    ->set(['flag' => 'mysql'])
			    ->where('_id', $id);
			    $updateStatus = $this->mongo_db->update('tmp_bc_response');
			    return $updateStatus;
	}
	public function log_record($data)
	{
		// $this->mongo_db->connect('barangkiriman');
		$insertId = $this->mongo_db->insert('log_message',$data);
		return $insertId;
	}
	public function respon_mongo($data)
	{
		$insertId = $this->mongo_db->insertAll('tmp_bc_response',$data);
		
    	return $insertId;
	}
	public function insert_getAll($data)
	{
		$insertId = $this->mongo_db->insert('tmp_bc_response',array_merge($data,['flag'=>'default']));
		
    	return $insertId;
	}
	public function insert_shipment_mongo($data)
	{
		$insertId = $this->mongo_db->insert('bc_shipment',array_merge($data,['flag'=>'default']));
		
    	return $insertId;
	}
	public function shipmentBK($data)
	{
		$insertId = $this->mongo_db->insert('bc_shipment_xml',array_merge($data,['flag'=>'default']));
		
    	return $insertId;
	}
	

}

/* End of file Bc_t_shipment_model.php */
/* Location: ./application/modules/bc_clearance/models/Bc_t_shipment_model.php */