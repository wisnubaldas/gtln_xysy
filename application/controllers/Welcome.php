<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('parser');
		$this->load->helper('js_css');
	}
	public function index()
	{	
		$data['js'] = [
						'admin.js',
						'pages/index.js',
						'demo.js'
						];
		$data['js_plugin'] = [
								$this->parser->theme_url('plugins/bootstrap-select/js/bootstrap-select.js'),
								$this->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js'),
								$this->parser->theme_url('plugins/jquery-countto/jquery.countTo.js'),
								$this->parser->theme_url('plugins/raphael/raphael.min.js'),
								$this->parser->theme_url('plugins/morrisjs/morris.js'),
								$this->parser->theme_url('plugins/chartjs/Chart.bundle.js'),
								$this->parser->theme_url('plugins/flot-charts/jquery.flot.js'),
								$this->parser->theme_url('plugins/flot-charts/jquery.flot.resize.js'),
								$this->parser->theme_url('plugins/flot-charts/jquery.flot.pie.js'),
								$this->parser->theme_url('plugins/flot-charts/jquery.flot.categories.js'),
								$this->parser->theme_url('plugins/flot-charts/jquery.flot.time.js'),
								$this->parser->theme_url('plugins/jquery-sparkline/jquery.sparkline.js'),
								];
		$data['css_plugin'] = [
								$this->parser->theme_url('plugins/morrisjs/morris.css'),
								];
		$data['css'] = ['style.css'];
        $data['title'] = "API";
        // Load the template from the views directory
        $this->parser->parse("index.tpl", $data);
	}
}
